# LZW

Простая программа для сжатия файлов по алгоритму Лемпеля-Зива-Велча.

Мы поддерживаем пароли, работаем с произвольными данными и кодируем в бинарные файлы в упакованном виде.
