import argparse
import getpass
from src.archive import Zipper, Unzipper
import os


def encode_method(args):
    password = getpass.getpass(
        prompt="Add password for archive: ",
    )

    if password == "":
        password = "abc"  # TODO

    files = []

    for path in args.files:
        if not os.path.exists(path):
            raise ValueError(f"path {path} not found")

        if os.path.isdir(path):
            for root, _, dirfiles in os.walk(path, topdown=True):
                for file in dirfiles:
                    files.append(os.path.join(root, file))

        else:
            files.append(path)

    Zipper(args.archive, files, password).zip()


def decode_method(args):
    for arch in args.archives:
        password = getpass.getpass(
            prompt=f"Enter password for {arch}: ",
        )

        if password == "":
            password = "abc"  # TODO

        Unzipper(arch, args.dir, password).unzip()


def main():
    parser = argparse.ArgumentParser(
        prog="LZW",
        description="Простая программа для сжатия файлов \
            по алгоритму Лемпеля-Зива-Велча.",
    )

    subparsers = parser.add_subparsers(required=True)

    encode_parser = subparsers.add_parser("encode")
    encode_parser.add_argument("files", nargs="+")
    encode_parser.add_argument("--archive", default="./out.lzw")
    encode_parser.set_defaults(method=encode_method)

    decode_parser = subparsers.add_parser("decode")
    decode_parser.add_argument("archives", nargs="+")
    decode_parser.add_argument("--dir", default=".")
    decode_parser.set_defaults(method=decode_method)

    args = parser.parse_args()
    args.method(args)


if __name__ == "__main__":
    main()
