from src.filesystem import BitInput, BitOutput


class LZWUnzipper:
    def __init__(
        self,
        alphabet_bits,
        input_file,
        input_file_size,
        output_file,
        output_file_size=0,
    ):
        self.__alphabet_bits = alphabet_bits

        self.__bit_input = BitInput(
            alphabet_bits,
            input_file,
            input_file_size,
        )
        self.__bit_output = BitOutput(
            alphabet_bits,
            output_file,
            output_file_size,
        )

    def decompress(self):
        max_code = 1 << self.__alphabet_bits

        codes = {x: (x, 1) for x in range(max_code)}

        current_code = max_code

        def index(value, index):
            index *= self.__alphabet_bits

            value >>= index
            value &= (1 << self.__alphabet_bits) - 1

            return value

        guess, guess_length = None, 0
        for bits, bits_length in self.__bit_input.bits():
            if guess is None:
                code, _ = codes[bits]
                self.__bit_output.bits(code)

                if current_code == max_code:
                    max_code <<= 1
                    self.__bit_input.increase_n_bits()

                guess, guess_length = code, 1
                continue

            value, value_length = None, None
            if bits in codes:
                value, value_length = codes[bits]
            else:
                value = guess

                value <<= self.__alphabet_bits
                value |= index(guess, guess_length - 1)

                value_length = guess_length + 1

            for i in range(value_length):
                shift_ = value_length - i - 1
                value_ = index(value, shift_)

                self.__bit_output.bits(value_)

            code = guess
            code <<= self.__alphabet_bits
            code |= index(value, value_length - 1)
            code_length = guess_length + 1

            codes[current_code] = (code, code_length)
            current_code += 1

            if current_code == max_code:
                max_code <<= 1
                self.__bit_input.increase_n_bits()

            guess, guess_length = value, value_length

        self.__bit_output.flush()
