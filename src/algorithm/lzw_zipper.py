from src.filesystem import BitInput, BitOutput


class LZWZipper:
    def __init__(
        self,
        alphabet_bits,
        input_file,
        input_file_size,
        output_file,
        output_file_size=0,
    ):
        self.__alphabet_bits = alphabet_bits

        self.__bit_input = BitInput(
            alphabet_bits,
            input_file,
            input_file_size,
        )
        self.__bit_output = BitOutput(
            alphabet_bits,
            output_file,
            output_file_size,
        )

    def compress(self):
        max_code = 1 << self.__alphabet_bits

        codes = {(x, 1): x for x in range(max_code)}

        current_code = max_code

        start, start_length = 0, 0
        for bits, bits_length in self.__bit_input.bits():
            start <<= self.__alphabet_bits
            start |= bits

            start_length += 1

            if (start, start_length) in codes:
                continue

            prefix = start >> self.__alphabet_bits
            prefix_length = start_length - 1

            code = codes[(prefix, prefix_length)]
            self.__bit_output.bits(code)

            if current_code == max_code:
                max_code <<= 1
                self.__bit_output.increase_n_bits()

            codes[(start, start_length)] = current_code
            current_code += 1

            start, start_length = bits, 1

        code = codes[(start, start_length)]
        self.__bit_output.bits(code)

        self.__bit_output.flush(write_part=True)

        return self.__bit_output.written()
