import hashlib
import tempfile
import itertools
import struct
import os
from src.algorithm import LZWUnzipper
from src.archive.constants import _PATH_LEN_SIZE_, _СONTENT_LEN_SIZE_, _CHUNK_SIZE_


class Unzipper:
    def __init__(self, input_file: str, out_dir: str, password: str):
        self.input_file = input_file
        self.out_dir = out_dir
        self.password = hashlib.sha512(password.encode()).hexdigest().encode()
        self.__l_bits = 0

    def __check_password(self, read_hash):
        read_hash = read_hash.decode()
        password_hash = hashlib.sha512(self.password).hexdigest()

        password_match = password_hash == read_hash

        assert password_match

    def __parse_archive_header(self, instream):
        return struct.unpack("128sB", instream.read(129))

    def __parse_entry_header(self, instream):
        path_len = struct.unpack("H", instream.read(_PATH_LEN_SIZE_))[0]
        path = instream.read(path_len).decode()
        content_len = struct.unpack("Q", instream.read(_СONTENT_LEN_SIZE_))[0]
        return path, content_len

    def __restore(self, tempfiles: list[str], tempdir: str):
        for tempfile in tempfiles:
            with open(os.path.join(tempdir, tempfile), "rb") as tempfile:
                path, content_len = self.__parse_entry_header(tempfile)
                basedir = os.path.dirname(path)
                basedir = os.path.join(self.out_dir, basedir)

                if not os.path.exists(basedir):
                    os.makedirs(basedir)

                with open(os.path.join(self.out_dir, path), "wb") as file:
                    l = LZWUnzipper(self.__l_bits, tempfile, content_len, file)
                    l.decompress()

    def unzip(self):
        with tempfile.TemporaryDirectory() as tempdir:
            self.__disassamble(tempdir)
            self.__restore(os.listdir(tempdir), tempdir)

    def __disassamble(self, tempdir: str):
        password = itertools.cycle(self.password)
        with open(self.input_file, "rb") as input:

            def decode(n_bytes):
                buffer = input.read(n_bytes)

                if len(buffer) == 0:
                    return

                buffer = bytes(a ^ b for a, b in zip(buffer, password))
                return buffer

            hash_header, self.__l_bits = self.__parse_archive_header(input)

            self.__check_password(hash_header)

            while (path_len_bytes := decode(_PATH_LEN_SIZE_)) is not None:
                path_len = struct.unpack("H", path_len_bytes)[0]
                path_bytes = decode(path_len)
                hash_path = hashlib.md5(path_bytes).hexdigest()
                content_bit_len_bytes = decode(_СONTENT_LEN_SIZE_)
                content_bit_len = struct.unpack("Q", content_bit_len_bytes)[0]
                content_byte_len = (content_bit_len + 7) // 8

                with open(os.path.join(tempdir, hash_path), "wb") as tempfile:
                    tempfile.write(path_len_bytes)
                    tempfile.write(path_bytes)
                    tempfile.write(content_bit_len_bytes)

                    while content_byte_len > 0:
                        chunk_len = min(_CHUNK_SIZE_, content_byte_len)
                        chunk = decode(chunk_len)
                        tempfile.write(chunk)
                        content_byte_len = content_byte_len - chunk_len
