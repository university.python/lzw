import tempfile
import os.path
import hashlib
import struct
import itertools
from src.algorithm import LZWZipper
from src.archive.constants import _СONTENT_LEN_SIZE_, _CHUNK_SIZE_

class Zipper:
    def __init__(self, output_file: str, file_list: list[str], password: str, n_bits=8):
        self.output_file = output_file
        self.file_list = file_list
        self.n_bits = n_bits
        self.password = hashlib.sha512(password.encode()).hexdigest().encode()

    def __form_archive_header(self, password, n_bits):
        password = hashlib.sha512(password).hexdigest().encode()
        return struct.pack("128sB", password, n_bits)

    def __form_entry_header(self, path):
        path = path.encode()
        path_length = len(path)

        header_format = "H{}s".format(path_length)
        header_format = struct.Struct(header_format)

        return header_format.pack(path_length, path)

    def __encode(self, chunck, password_iterator):
        return bytes(a ^ b for a, b in zip(chunck, password_iterator))

    def __store(self, tempdir, files):
        for file in files:
            relpath = os.path.relpath(file)
            relpath_encoded = relpath.encode()

            tempfile = hashlib.sha512(relpath_encoded).hexdigest()
            tempfile = os.path.join(tempdir, tempfile)

            with open(file, "rb") as input, open(tempfile, "wb") as output:
                entry_header = self.__form_entry_header(relpath)
                output.write(entry_header)

                compressed_size_start = output.tell()

                output.seek(_СONTENT_LEN_SIZE_, 1)

                input_size = os.path.getsize(file) * 8

                compressed_size = LZWZipper(
                    self.n_bits,
                    input,
                    input_size,
                    output,
                ).compress()

                compressed_size = struct.pack("Q", compressed_size)

                output.seek(compressed_size_start, 0)
                output.write(compressed_size)

    def __assemble(self, tempdir):
        with open(self.output_file, "wb") as output_file:
            archive_header = self.__form_archive_header(self.password, self.n_bits)
            output_file.write(archive_header)

            password_iterator = itertools.cycle(self.password)
            for input_filename in os.listdir(tempdir):
                input_filename = os.path.join(tempdir, input_filename)

                with open(input_filename, "rb") as input:
                    while chunk := input.read(_CHUNK_SIZE_):
                        chunk = self.__encode(chunk, password_iterator)

                        output_file.write(chunk)

    def zip(self):
        with tempfile.TemporaryDirectory() as tempdir:
            self.__store(tempdir, self.file_list)
            self.__assemble(tempdir)
    