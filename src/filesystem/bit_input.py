class BitInput:
    def __init__(self, n_bits, file, size):
        self.__n_bits = n_bits

        self.__file = file
        self.__size = size
        self.__written = 0

    def __bytes(self):
        BLOCK_SIZE = 1024

        while block := self.__file.read(BLOCK_SIZE):
            yield from block

    def n_bits(self):
        return self.__n_bits

    def written(self):
        return self.__written

    def size(self):
        return self.__size

    def increase_n_bits(self):
        self.__n_bits += 1

    def bits(self):
        BITS_IN_BYTE = 8
        bytes_ = self.__bytes()

        unget, unget_length = 0, 0
        while self.__written < self.__size:
            bits, bits_length = 0, 0

            if unget_length > 0:
                bits, bits_length = unget, unget_length

                unget_length, unget = 0, 0

            while bits_length < self.n_bits():
                try:
                    next_bits = next(bytes_)
                except StopIteration:
                    break

                bits <<= BITS_IN_BYTE
                bits |= next_bits
                bits_length += BITS_IN_BYTE

            if bits_length > self.n_bits():
                unget_length = bits_length - self.n_bits()

                unget = bits
                unget &= (1 << unget_length) - 1

                bits >>= unget_length
                bits_length -= unget_length

            if bits_length == 0:
                break

            self.__written += bits_length

            yield (bits, bits_length)
