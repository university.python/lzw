import logging


class BitOutput:
    def __init__(self, n_bits, file, size):
        self.__n_bits = n_bits

        self.__file = file
        self.__size = size
        self.__written = 0

        self.__buffer = bytearray()
        self.__buffer_length = 0

    def n_bits(self):
        return self.__n_bits

    def written(self):
        return self.__written

    def size(self):
        return self.__size

    def increase_n_bits(self):
        self.__n_bits += 1

    def bits(self, bits):
        BITS_IN_BYTE = 8
        BLOCK_SIZE = 1024 * BITS_IN_BYTE

        if self.__buffer_length > BLOCK_SIZE:
            self.flush()

        written_length = 0

        unused_bits = self.__buffer_length % BITS_IN_BYTE
        unused_bits = BITS_IN_BYTE - unused_bits
        unused_bits %= BITS_IN_BYTE

        if self.n_bits() <= unused_bits:
            byte = bits
            byte <<= unused_bits - self.n_bits()
            byte &= (1 << unused_bits) - 1

            self.__buffer[-1] |= byte
            self.__buffer_length += self.n_bits()

            written_length += self.n_bits()
        elif self.n_bits() > unused_bits > 0:
            byte = bits
            byte >>= self.n_bits() - unused_bits
            byte &= (1 << unused_bits) - 1

            self.__buffer[-1] |= byte
            self.__buffer_length += unused_bits

            written_length += unused_bits

        while written_length + BITS_IN_BYTE <= self.n_bits():
            byte = bits
            byte >>= self.n_bits() - (written_length + BITS_IN_BYTE)
            byte &= (1 << BITS_IN_BYTE) - 1

            self.__buffer.append(byte)
            self.__buffer_length += BITS_IN_BYTE

            written_length += BITS_IN_BYTE

        if written_length < self.n_bits():
            byte = bits
            byte <<= BITS_IN_BYTE - ((self.n_bits() - written_length) % BITS_IN_BYTE)
            byte &= (1 << BITS_IN_BYTE) - 1

            self.__buffer.append(byte)
            self.__buffer_length += self.n_bits() - written_length

            written_length += self.n_bits() - written_length

        return written_length

    def flush(self, write_part=False):
        BITS_IN_BYTE = 8

        last, last_length = None, 0

        if not write_part and self.__buffer_length % BITS_IN_BYTE != 0:
            last = self.__buffer.pop()
            last_length = self.__buffer_length % BITS_IN_BYTE

            self.__buffer_length -= last_length

        logging.debug(f"flush bits {self.__buffer_length}: {self.__buffer.hex()}")
        self.__file.write(self.__buffer)
        self.__written += self.__buffer_length

        self.__buffer.clear()
        self.__buffer_length = 0

        if last is not None:
            self.__buffer.append(last)
            self.__buffer_length = last_length
