import unittest, logging, tempfile

from src.bit_operations import BitInput, BitOutput


class BitOperationsTest(unittest.TestCase):
    def setUp(self):
        super().setUp()

        logging.basicConfig(level=logging.NOTSET)

    def test_number_to_string(self):
        with tempfile.TemporaryFile("rb+", buffering=0) as file:
            number = 0x696D706F7274  # Would be "import"

            bits_out = BitOutput(file, 6 * 8)
            bits_out.bits(number)
            bits_out.flush()

            print(file.name)
            file.truncate()

            bits_in = BitInput(file, 8)
            content = [chr(x) for x in bits_in.bits()]
            content = "".join(content)

        self.assertEqual(content, "import")
